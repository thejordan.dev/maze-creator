package fr.thejordan.maze;

import org.bukkit.entity.Player;


import lombok.Getter;

public class Helper {

	public static EasyExcpetion printExcpetion(Exception e) {
		EasyExcpetion exception = new EasyExcpetion(e);
		System.out.println("\n");
		System.out.println("=============================");
		System.out.println("-=["+exception.getName()+"]=-");
		System.out.println("\n");
		exception.printError();
		System.out.println("\n");
		System.out.println("=============================");
		System.out.println("\n");
		return exception;
	}
	
	public static class EasyExcpetion {
		
		@Getter private Exception e;
		
		public EasyExcpetion(Exception e) {
			this.e = e;
		}
		
		public String getName() { return e.getClass().getName(); }
		public String getMessage() { return e.getMessage(); }
		public String getLocalizedMessage() { return e.getLocalizedMessage(); }
		public StackTraceElement[] getElements() { return e.getStackTrace(); }
		
		public void printError() {
			for (StackTraceElement el : getElements()) {
				System.out.println("["+el.getClassName()+" ; "+el.getLineNumber()+"] "+el.getMethodName());
			}
		}
		
		public static EasyExcpetion create(Exception e) {
			return new EasyExcpetion(e);
		}
		
	}

	public static void sendHelp(Player sender) {
		sender.sendMessage("�9-=-=-= MAZE CREATOR =-=-=-");
		sender.sendMessage("�7�oby TheSkinter/TheJordan");
		sender.sendMessage("�b/maze create <taille>");
		sender.sendMessage("�b/maze create <largeur> <longueur>");
		sender.sendMessage("�9-=-=-=-=-=|  |=-=-=-=-=-=- ");
	}
	
	public static boolean isStringInt(String number) {
		try {
			Integer.parseInt(number);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static int stringToInt(String number) {
		try {
			return Integer.parseInt(number);
		} catch (Exception e) {
			return -1;
		}
	}
	
}
