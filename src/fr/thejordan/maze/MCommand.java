package fr.thejordan.maze;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import fr.thejordan.maze.Structure.Direction;

public class MCommand implements CommandExecutor, TabCompleter {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) return false;
		if (args.length == 0) { Helper.sendHelp((Player)sender); }
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("remove")) {
				List<Structure> structures = MazePlugin.instance.getRegionIn(((Player)sender).getLocation());
				if (structures.isEmpty()) return false;
				structures.get(0).destroy();
			} else if (args[0].equalsIgnoreCase("reveal")) {
				List<Structure> structures = MazePlugin.instance.getRegionIn(((Player)sender).getLocation());
				if (structures.isEmpty()) return false;
				Structure s = structures.get(0);
				if (s.getViewers().contains(((Player)sender).getUniqueId())) {
					s.getViewers().remove(((Player)sender).getUniqueId());
				} else {
					s.getViewers().add(((Player)sender).getUniqueId());
				}
			}
		}
		if (args.length == 2) {
			if (args[0].equalsIgnoreCase("create")) {
				if (Helper.isStringInt(args[1])) {
					int size = Helper.stringToInt(args[1]);
					if (size >= 2) {
						Maze maze = new Maze(size);
						maze.pasteInMinecraft(((Player)sender).getLocation(), Direction.fromYaw(((Player)sender).getLocation().getYaw()));
					}
				}
			}else if (args[0].equalsIgnoreCase("reveal")) {
				if (Bukkit.getPlayerExact(args[1]) != null) {
					Player player = Bukkit.getPlayerExact(args[1]);
					List<Structure> structures = MazePlugin.instance.getRegionIn(((Player)sender).getLocation());
					if (structures.isEmpty()) return false;
					Structure s = structures.get(0);
					if (s.getViewers().contains(player.getUniqueId())) {
						s.getViewers().remove(player.getUniqueId());
					} else {
						s.getViewers().add(player.getUniqueId());
					}
				}
			}
		}
		if (args.length == 3) {
			if (args[0].equalsIgnoreCase("create")) {
				if (Helper.isStringInt(args[1])) {
					if (Helper.isStringInt(args[2])) {
						int larg = Helper.stringToInt(args[1]);
						int log = Helper.stringToInt(args[2]);
						if (larg >= 2 && log >= 2) {
							Maze maze = new Maze(larg,log);
							maze.pasteInMinecraft(((Player)sender).getLocation(), Direction.fromYaw(((Player)sender).getLocation().getYaw()));
						}
					} else if (args[2].equalsIgnoreCase("true")) {
						int size = Helper.stringToInt(args[1]);
						if (size >= 2) {
							Maze maze = new Maze(size);
							sender.sendMessage("PATH");
							maze.pasteInMinecraft(((Player)sender).getLocation(), Direction.fromYaw(((Player)sender).getLocation().getYaw()),true);
						}
					}
				}
			}
		} else if (args.length == 4) {
			if (args[0].equalsIgnoreCase("create")) {
				if (Helper.isStringInt(args[1])) {
					if (Helper.isStringInt(args[2])) {
						if (args[3].equalsIgnoreCase("true")) {
							int larg = Helper.stringToInt(args[1]);
							int log = Helper.stringToInt(args[2]);
							if (larg >= 2 && log >= 2) {
								Maze maze = new Maze(larg,log);
								sender.sendMessage("PATH");
								maze.pasteInMinecraft(((Player)sender).getLocation(), Direction.fromYaw(((Player)sender).getLocation().getYaw()),true);
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		List<String> returned = new ArrayList<String>();
		return returned;
	}
}
