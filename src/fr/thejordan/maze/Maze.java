package fr.thejordan.maze;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;

import fr.thejordan.maze.Structure.Direction;
import fr.thejordan.maze.Structure.Relative;

public class Maze {
    private int dimensionX, dimensionY; // dimension of maze
    private int gridDimensionX, gridDimensionY; // dimension of output grid
    private char[][] grid; // output grid
    private Cell[][] cells; // 2d array of Cells
    private Random random = new Random(); // The random object

    public Maze(int aDimension) { this(aDimension, aDimension); }

    public Maze(int xDimension, int yDimension) {
        dimensionX = (xDimension/2);
        dimensionY = yDimension;
        gridDimensionX = dimensionX * 4 + 1;
        gridDimensionY = dimensionY * 2 + 1;
        grid = new char[gridDimensionX][gridDimensionY];
        init();
        generateMaze();
    }

    private void init() {
        cells = new Cell[dimensionX][dimensionY];
        for (int x = 0; x < dimensionX; x++) {
            for (int y = 0; y < dimensionY; y++) {
                cells[x][y] = new Cell(x, y, false); // create cell (see Cell constructor)
            }
        }
    }

    private class Cell {
        int x, y; // coordinates
        ArrayList < Cell > neighbors = new ArrayList <>(); boolean visited = false;
        Cell parent = null; boolean inPath = false;
        double travelled; double projectedDist;
        boolean wall = true; boolean open = true;
        Cell(int x, int y) {
            this(x, y, true);
        }
        Cell(int x, int y, boolean isWall) {
            this.x = x;
            this.y = y;
            this.wall = isWall;
        }
        void addNeighbor(Cell other) {
            if (!this.neighbors.contains(other)) { this.neighbors.add(other); }
            if (!other.neighbors.contains(this)) { other.neighbors.add(this); }
        }

        boolean isCellBelowNeighbor() { return this.neighbors.contains(new Cell(this.x, this.y + 1)); }

        boolean isCellRightNeighbor() { return this.neighbors.contains(new Cell(this.x + 1, this.y)); }

        @Override
        public String toString() { return String.format("Cell(%s, %s)", x, y); }

        @Override
        public boolean equals(Object other) {
            if (!(other instanceof Cell)) return false;
            Cell otherCell = (Cell) other;
            return (this.x == otherCell.x && this.y == otherCell.y);
        }
        
        
        @Override
        public int hashCode() { return this.x + this.y * 256; }
    }

    private void generateMaze() { generateMaze(0, 0); }

    private void generateMaze(int x, int y) { generateMaze(getCell(x, y)); }
    
    private void generateMaze(Cell startAt) {
        if (startAt == null) return;
        startAt.open = false; // indicate cell closed for generation
        ArrayList < Cell > cells = new ArrayList < > ();
        cells.add(startAt);

        while (!cells.isEmpty()) {
            Cell cell;
            if (random.nextInt(10) == 0)
                cell = cells.remove(random.nextInt(cells.size()));
            else cell = cells.remove(cells.size() - 1);
            ArrayList < Cell > neighbors = new ArrayList < > ();
            Cell[] potentialNeighbors = new Cell[] {
                getCell(cell.x + 1, cell.y),
                    getCell(cell.x, cell.y + 1),
                    getCell(cell.x - 1, cell.y),
                    getCell(cell.x, cell.y - 1)
            };
            for (Cell other: potentialNeighbors) {
                if (other == null || other.wall || !other.open) continue;
                neighbors.add(other);
            }
            if (neighbors.isEmpty()) continue;
            Cell selected = neighbors.get(random.nextInt(neighbors.size()));
            selected.open = false; // indicate cell closed for generation
            cell.addNeighbor(selected);
            cells.add(cell);
            cells.add(selected);
        }
    }
    public Cell getCell(int x, int y) {
        try { return cells[x][y];
        } catch (ArrayIndexOutOfBoundsException e) { return null; }
    }

    public void solve() { this.solve(0, 0, dimensionX - 1, dimensionY - 1); }
    public void solve(int startX, int startY, int endX, int endY) {
        for (Cell[] cellrow: this.cells) {
            for (Cell cell: cellrow) {
                cell.parent = null;
                cell.visited = false;
                cell.inPath = false;
                cell.travelled = 0;
                cell.projectedDist = -1;
            }
        }
        ArrayList < Cell > openCells = new ArrayList < > ();
        Cell endCell = getCell(endX, endY);
        if (endCell == null) return;
        { 
            Cell start = getCell(startX, startY);
            if (start == null) return; // quit if start out of bounds
            start.projectedDist = getProjectedDistance(start, 0, endCell);
            start.visited = true;
            openCells.add(start);
        }
        boolean solving = true;
        while (solving) {
            if (openCells.isEmpty()) return; // quit, no path
            Collections.sort(openCells, new Comparator < Cell > () {
                @Override
                public int compare(Cell cell1, Cell cell2) {
                    double diff = cell1.projectedDist - cell2.projectedDist;
                    if (diff > 0) return 1;
                    else if (diff < 0) return -1;
                    else return 0;
                }
            });
            Cell current = openCells.remove(0); // pop cell least projectedDist
            if (current == endCell) break; // at end
            for (Cell neighbor: current.neighbors) {
                double projDist = getProjectedDistance(neighbor,
                    current.travelled + 1, endCell);
                if (!neighbor.visited || // not visited yet
                    projDist < neighbor.projectedDist) { // better path
                    neighbor.parent = current;
                    neighbor.visited = true;
                    neighbor.projectedDist = projDist;
                    neighbor.travelled = current.travelled + 1;
                    if (!openCells.contains(neighbor))
                        openCells.add(neighbor);
                }
            }
        }
        // create path from end to beginning
        Cell backtracking = endCell;
        backtracking.inPath = true;
        while (backtracking.parent != null) {
            backtracking = backtracking.parent;
            backtracking.inPath = true;
        }
    }
    // get the projected distance
    // (A star algorithm consistent)
    public double getProjectedDistance(Cell current, double travelled, Cell end) {
        return travelled + Math.abs(current.x - end.x) +
            Math.abs(current.y - current.x);
    }

    // draw the maze
    public void updateGrid() {
        char backChar = '#', wallChar = 'X', cellChar = ' ', pathChar = '+';
        // fill background
        for (int x = 0; x < gridDimensionX; x++) {
            for (int y = 0; y < gridDimensionY; y++) {
                grid[x][y] = backChar;
            }
        }
        // build walls
        for (int x = 0; x < gridDimensionX; x++) {
            for (int y = 0; y < gridDimensionY; y++) {
                if (x % 4 == 0 || y % 2 == 0)
                    grid[x][y] = wallChar;
            }
        }
        // make meaningful representation
        for (int x = 0; x < dimensionX; x++) {
            for (int y = 0; y < dimensionY; y++) {
                Cell current = getCell(x, y);
                int gridX = x * 4 + 2, gridY = y * 2 + 1;
                if (current.inPath) {
                    grid[gridX][gridY] = pathChar;
                    if (current.isCellBelowNeighbor())
                        if (getCell(x, y + 1).inPath) {
                            grid[gridX][gridY + 1] = pathChar;
                            grid[gridX + 1][gridY + 1] = backChar;
                            grid[gridX - 1][gridY + 1] = backChar;
                        } else {
                            grid[gridX][gridY + 1] = cellChar;
                            grid[gridX + 1][gridY + 1] = backChar;
                            grid[gridX - 1][gridY + 1] = backChar;
                        }
                    if (current.isCellRightNeighbor())
                        if (getCell(x + 1, y).inPath) {
                            grid[gridX + 2][gridY] = pathChar;
                            grid[gridX + 1][gridY] = pathChar;
                            grid[gridX + 3][gridY] = pathChar;
                        } else {
                            grid[gridX + 2][gridY] = cellChar;
                            grid[gridX + 1][gridY] = cellChar;
                            grid[gridX + 3][gridY] = cellChar;
                        }
                } else {
                    grid[gridX][gridY] = cellChar;
                    if (current.isCellBelowNeighbor()) {
                        grid[gridX][gridY + 1] = cellChar;
                        grid[gridX + 1][gridY + 1] = backChar;
                        grid[gridX - 1][gridY + 1] = backChar;
                    }
                    if (current.isCellRightNeighbor()) {
                        grid[gridX + 2][gridY] = cellChar;
                        grid[gridX + 1][gridY] = cellChar;
                        grid[gridX + 3][gridY] = cellChar;
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        //updateGrid();
        String output = "";
        for (int y = 0; y < gridDimensionY; y++) {
            for (int x = 0; x < gridDimensionX; x++) {
                output += grid[x][y];
            }
            output += "\n";
        }
        return output;
    }

    public Structure pasteInMinecraft(Location to,Direction rotation) {
    	return pasteInMinecraft(to, rotation, false);
    }
    public Structure pasteInMinecraft(Location to,Direction rotation,boolean showPath) {
    	ArrayList<Relative> walls = new ArrayList<>();
    	ArrayList<Relative> path = new ArrayList<>();
    	ArrayList<Relative> air = new ArrayList<>();
        Structure s = new Structure(this,to,rotation);
    	solve();
    	updateGrid();
    	
    	grid[2][0] = '0';
    	grid[gridDimensionX-1][gridDimensionY-2] = ' ';
        for (int y = 0; y < gridDimensionY; y++) {
            for (int x = 0; x < gridDimensionX; x++) {
            	Relative relative = new Relative(x-2, 0, y);
                if(grid[x][y] == 'X') {
                	if (x == 0 && y == 0) { relative.setFirst(true); }
                	else if (x == gridDimensionX-1 && y == gridDimensionY-1) { relative.setLast(true); }
                	walls.add(relative);
                } else if(grid[x][y] == '+') {
                	path.add(relative);
                } else if (grid[x][y] == ' ') {
                	air.add(relative);
                }
            }
        }
        s.getWalls().addAll(walls);
        s.getPath().addAll(path);
        s.getAir().addAll(air);
        s.rotateWithDir();
        List<Relative> whole = new ArrayList<>();
        whole.addAll(walls); whole.addAll(air);
        s.setup(whole);
        return s;
    }
    
}