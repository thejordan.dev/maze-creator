package fr.thejordan.maze;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;


public class MazePlugin extends JavaPlugin  {

	public static MazePlugin instance;
	@Getter private List<Structure> structures = new ArrayList<>();
	@Getter private PathShowerRunnable path = new PathShowerRunnable();
	
	@Override
	public void onEnable() {
		instance = this;
		this.path = new PathShowerRunnable();
		this.path.runTaskTimer(this, 0L, 10L);
		getCommand("maze").setExecutor(new MCommand()); getCommand("maze").setTabCompleter(new MCommand());
	}
	
	public List<Structure> getRegionIn(Location loc) {
		List<Structure> regions = new ArrayList<>();
		for (Structure region : getStructures()) {
			if (region.isIn(loc)) {
				regions.add(region);
			}
		}
		return regions;
	}
	
	@Override
	public void onDisable() {
		List<Structure> it = new ArrayList<>();
		for (Structure structure : getStructures()) { it.add(structure); }
		for (Structure st : it) { st.destroy(); }
		
	}
	
}
