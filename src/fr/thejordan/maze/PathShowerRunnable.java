package fr.thejordan.maze;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import fr.thejordan.maze.Structure.Relative;

public class PathShowerRunnable extends BukkitRunnable {

	@Override
	public void run() {
		Map<Structure,List<UUID>> offlines = new HashMap<>();
		for (Structure structure : MazePlugin.instance.getStructures()) {
			for (UUID pID : structure.getViewers()) {
				OfflinePlayer off = Bukkit.getOfflinePlayer(pID);
				if (off != null && off.hasPlayedBefore() && off.isOnline()) {
					Player player = off.getPlayer();
					for (Relative point : structure.getPath()) {
						Location pointLocation = structure.getSource().clone().add(point.getX(),0.5,point.getZ());
						player.spawnParticle(Particle.END_ROD, pointLocation,10,0.2,0,0.2,0D);
					}
				} else {
					if (!offlines.containsKey(structure)) offlines.put(structure, new ArrayList<>());
					offlines.get(structure).add(pID);
				}
			}
			if (offlines.containsKey(structure)) structure.getViewers().removeAll(offlines.get(structure));
		}
	}

}
