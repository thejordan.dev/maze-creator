package fr.thejordan.maze;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;

import lombok.Getter;
import lombok.Setter;

public class Structure {

	@Getter private UUID uuid;
	@Getter private Location source;
	@Getter private Direction direction;
	@Getter private boolean showingPath = false;
	@Getter private ArrayList<Relative> walls = new ArrayList<>();
	@Getter private ArrayList<Relative> path = new ArrayList<>();
	@Getter private ArrayList<Relative> air = new ArrayList<>();
	@Getter private Map<Location,BlockData> backupLand = new HashMap<>();
	@Getter private Location first;
	@Getter private Location last;
	@Getter private Maze currentMaze;
	@Getter private List<UUID> viewers = new ArrayList<>();
	
	public Structure(Maze maze,Location source,Direction dir) {
		this.uuid = UUID.randomUUID();
		this.source = source;
		this.direction = dir;
		this.currentMaze = maze;
		MazePlugin.instance.getStructures().add(this);
	}
	
	public void rotateWithDir() {
		for (Relative walls : getWalls()) {
			double rX = walls.getX();
			double rZ = walls.getZ();
			switch (direction) {
				case NORTH: walls.setX(-rX); walls.setZ(-rZ); break;
				case SOUTH: break;
				case EAST: walls.setX(-rZ); walls.setZ(rX); break;
				case WEST: walls.setX(rZ); walls.setZ(-rX); break;
			}
		}
		for (Relative path : getPath()) {
			double rX = path.getX();
			double rZ = path.getZ();
			switch (direction) {
				case NORTH: path.setX(-rX);	path.setZ(-rZ); break;
				case SOUTH: break;
				case EAST: path.setX(-rZ); path.setZ(rX); break;
				case WEST: path.setX(rZ); path.setZ(-rX); break;
			}
		}
	}
	
	public void setup(List<Relative> maze) {
		for (Relative relative : maze) {
			if (relative.isFirst) this.first = source.clone().add(relative.x,4,relative.z);
			if (relative.isLast) this.last = source.clone().add(relative.x,0,relative.z);
		}
		backupWorld();
		fill(first, last, Material.AIR);
		parse();
	}
	
	public void backupWorld() {
		for(int x = (int) Math.max(first.getBlockX(), last.getBlockX()); x >= (int) Math.min(last.getBlockX(), first.getBlockX()); x--) {
		    for(int y = (int) Math.max(first.getBlockY(), last.getBlockY()); y >= (int) Math.min(last.getBlockY(), first.getBlockY()); y--) {
		        for(int z = (int) Math.max(first.getBlockZ(), last.getBlockZ()); z >= (int) Math.min(last.getBlockZ(), first.getBlockZ()); z--) {
		        	Block block = first.getWorld().getBlockAt(x, y, z); getBackupLand().put(block.getLocation(), block.getBlockData());
		        }
		    }
		}
	}
	
	public void parse() {
		for (Relative relative : getWalls()) {
			Location loc = getSource().clone();
			loc.add(relative.x, 0, relative.z);
			for (int i = 0;i<4;i++) {
				loc.add(0, i, 0);
				loc.getBlock().setType(Material.DARK_OAK_LOG);
				loc.subtract(0, i, 0);
			}
		}
		if (first != null && last != null)  fill(first,last.clone().add(0, 4, 0),Material.BARRIER);	
	}
	
	public void fill(Location max,Location min, Material material) {
		if (!max.getWorld().getName().equals(min.getWorld().getName())) return;
		for(int x = (int) Math.max(max.getBlockX(), min.getBlockX()); x >= (int) Math.min(min.getBlockX(), max.getBlockX()); x--) {
		    for(int y = (int) Math.max(max.getBlockY(), min.getBlockY()); y >= (int) Math.min(min.getBlockY(), max.getBlockY()); y--) {
		        for(int z = (int) Math.max(max.getBlockZ(), min.getBlockZ()); z >= (int) Math.min(min.getBlockZ(), max.getBlockZ()); z--) {
		        	Block block = max.getWorld().getBlockAt(x, y, z); block.setType(material);
		        }
		    }
		}
	}
	
    public enum Direction {
    	NORTH(), SOUTH(), EAST(), WEST();

    	private Direction() { }
    	
    	public static Direction fromYaw(float yaw) {
    		final Direction[] axis = { SOUTH, EAST, NORTH, WEST };
    		return axis[Math.round(yaw / 90f) & 0x3];
    	}
    }
    
    public static class Relative {
    	@Setter @Getter private boolean isFirst = false;
    	@Setter @Getter private boolean isLast = false;
    	@Setter @Getter private double x,y,z;
    	
    	public Relative(int x,int y,int z) { this.x = x; this.y = y; this.z = z; }
    	public Relative(double x,double y,double z) { this.x = x; this.y = y; this.z = z; }

    }
	
	public boolean isIn(Location paramLocation) {
		if (paramLocation.getWorld() != source.getWorld()) { return false; }
		int xMax = Math.max(first.getBlockX(), last.getBlockX());
		int xMin = Math.min(last.getBlockX(), first.getBlockX());
		if (paramLocation.getBlockX() < xMin) { return false; }
		if (paramLocation.getBlockX() > xMax) { return false; }
		int zMax = Math.max(first.getBlockZ(), last.getBlockZ());
		int zMin = Math.min(last.getBlockZ(), first.getBlockZ());
		if (paramLocation.getBlockZ() < zMin) { return false; }
		if (paramLocation.getBlockZ() > zMax) { return false; }
		//int yMax = Math.max(first.getBlockY(), last.getBlockY());
		//int yMin = Math.min(last.getBlockY(), first.getBlockY());
		/*if (paramLocation.getBlockY() < yMin) { return false; }
		if (paramLocation.getBlockY() > yMax) { return false; }*/
		return true;
	}
	
	public void destroy() {
		fill(first, last, Material.AIR);
		for (Location relative : getBackupLand().keySet()) {
			BlockData data = getBackupLand().get(relative);
			relative.getBlock().setBlockData(data);
		}
		MazePlugin.instance.getStructures().remove(this);
	}
    
}
